package io.groggs.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.google.common.net.MediaType.*;

public class ContentTypes {
    private static final Map<String, String> CONTENT_TYPE_BY_EXTENSION = new HashMap<>();

    static {
        CONTENT_TYPE_BY_EXTENSION.put("pdf", PDF.toString());
        CONTENT_TYPE_BY_EXTENSION.put("txt", PLAIN_TEXT_UTF_8.toString());
        CONTENT_TYPE_BY_EXTENSION.put("mp4", MP4_VIDEO.toString());
        //TBD
    }

    public static String getContentType(File file) {
        String extension = file.getName().substring(file.getName().lastIndexOf('.') + 1);
        return getPredefinedTypeByExtension(extension)
                .orElse(getTypeFromFile(file)
                        .orElse(ANY_TYPE.toString()));
    }

    private static Optional<String> getPredefinedTypeByExtension(String extension) {
        return Optional.ofNullable(CONTENT_TYPE_BY_EXTENSION.get(extension));
    }

    private static Optional<String> getTypeFromFile(File file) {
        try {
            return Optional.ofNullable(Files.probeContentType(Paths.get(file.getPath())));
        } catch (IOException ignored) {
            return Optional.empty();
        }
    }
}
