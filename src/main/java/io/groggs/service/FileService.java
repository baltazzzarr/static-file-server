package io.groggs.service;

import java.io.File;

public interface FileService {
    String getFullPath(String uri);
    boolean isAllowedFileName(String name);
    String getContentType(File file);
}
