package io.groggs.service;

import io.groggs.configuration.PropertySource;
import io.netty.util.CharsetUtil;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Paths;
import java.util.regex.Pattern;

public class LocalFileService implements FileService {
    private static final Pattern ALLOWED_FILE_NAME = Pattern.compile("[^-\\._]?[^<>&\\\"]*");

    private String root;

    public LocalFileService(PropertySource propertySource) {
        this.root = propertySource.getPropertyAsString("root.path");
    }

    @Override
    public String getFullPath(String uri) {
        String decodedUri;
        try {
            decodedUri = URLDecoder.decode(uri, CharsetUtil.UTF_8.name())
                    .replace('/', File.separatorChar);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        //TODO: sanitize received uri

        return root + Paths.get(decodedUri).normalize();
    }

    @Override
    public boolean isAllowedFileName(String name) {
        return ALLOWED_FILE_NAME.matcher(name).matches();
    }

    @Override
    public String getContentType(File file) {
        return ContentTypes.getContentType(file);
    }
}
