package io.groggs;

import io.groggs.configuration.PropertySource;
import io.groggs.server.StaticFileServer;

public class StaticFileServerApp {
    public static void main(String[] args) {
        PropertySource propertySource = new PropertySource();
        new StaticFileServer(propertySource).start();
    }
}
