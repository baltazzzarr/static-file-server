package io.groggs.configuration;

class PropertyNotFoundException extends RuntimeException {
    PropertyNotFoundException(String s) {
        super(s);
    }
}
