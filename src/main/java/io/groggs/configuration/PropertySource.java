package io.groggs.configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

public class PropertySource {

    private final Map<String, String> defaultProperties = new HashMap<>();

    public PropertySource() {
        try {
            Properties serverProperties = new Properties();
            serverProperties.load(getClass().getClassLoader().getResourceAsStream("server.properties"));
            serverProperties.forEach((key, value) -> {
                if (System.getProperty(key.toString()) == null) {
                    System.setProperty(key.toString(), value.toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        initDefaultProperties();
    }

    private void initDefaultProperties() {
        defaultProperties.put("root.path", System.getProperty("user.dir"));
    }

    public String getPropertyAsString(String propertyName) {
        return getSystemProperty(propertyName)
                .orElseGet(() -> getDefaultProperty(propertyName));
    }

    public int getPropertyAsInteger(String propertyName) {
        return Integer.valueOf(getPropertyAsString(propertyName));
    }

    private Optional<String> getSystemProperty(String propertyName) {
        return Optional.ofNullable(System.getProperty(propertyName));
    }

    private String getDefaultProperty(String propertyName) {
        return Optional.ofNullable(defaultProperties.get(propertyName))
                .orElseThrow(() -> new PropertyNotFoundException(propertyName + " is not set"));
    }
}
