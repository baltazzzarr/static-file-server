package io.groggs.server;

import io.groggs.server.request.err.HttpException;
import io.groggs.server.request.handler.RequestHandler;
import io.groggs.server.request.handler.WritableHttpResponse;
import io.groggs.server.request.log.AccessLogger;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StaticFileServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private static final Logger LOG = LogManager.getLogger(StaticFileServerHandler.class);

    private RequestHandler requestHandler;
    private HttpRequest request;

    public StaticFileServerHandler(RequestHandler requestHandler) {
        this.requestHandler = requestHandler;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest req) {
        this.request = req;
        WritableHttpResponse writableResponse = requestHandler.process(request);
        writableResponse.writeResponse(ctx);
        AccessLogger.log(request, writableResponse.getHttpResponse());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        HttpResponseStatus status = cause instanceof HttpException ? ((HttpException) cause).getStatus() :
                HttpResponseStatus.INTERNAL_SERVER_ERROR;
        WritableHttpResponse writableResponse = requestHandler.error(status);
        writableResponse.writeResponse(ctx);
        AccessLogger.log(request, writableResponse.getHttpResponse());
        if (status == HttpResponseStatus.INTERNAL_SERVER_ERROR) {
            LOG.error("Internal error", cause);
        }
    }
}
