package io.groggs.server;

import io.groggs.configuration.PropertySource;
import io.groggs.server.request.handler.RequestHandler;
import io.groggs.server.request.handler.SimpleRequestHandler;
import io.groggs.service.LocalFileService;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

public class StaticFileServerInitializer extends ChannelInitializer<SocketChannel> {

    private RequestHandler requestHandler;

    StaticFileServerInitializer(PropertySource propertySource) {
        this.requestHandler = new SimpleRequestHandler(new LocalFileService(propertySource));
    }

    @Override
    protected void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new HttpServerCodec())
                .addLast(new HttpObjectAggregator(65536))
                .addLast(new ChunkedWriteHandler())
                .addLast(new StaticFileServerHandler(requestHandler));
    }
}
