package io.groggs.server.request.err;

import io.netty.handler.codec.http.HttpResponseStatus;

public class ForbiddenHttpException extends HttpException {
    public ForbiddenHttpException() {
        super(HttpResponseStatus.FORBIDDEN);
    }

    public ForbiddenHttpException(String s) {
        super(s, HttpResponseStatus.FORBIDDEN);
    }
}
