package io.groggs.server.request.err;

import io.netty.handler.codec.http.HttpResponseStatus;

public class MethodNotAllowedHttpException extends HttpException {

    public MethodNotAllowedHttpException() {
        super(HttpResponseStatus.METHOD_NOT_ALLOWED);
    }

    public MethodNotAllowedHttpException(String s) {
        super(s, HttpResponseStatus.METHOD_NOT_ALLOWED);
    }
}
