package io.groggs.server.request.err;

import io.netty.handler.codec.http.HttpResponseStatus;

public class NotFoundHttpException extends HttpException {
    public NotFoundHttpException() {
        super(HttpResponseStatus.NOT_FOUND);
    }

    public NotFoundHttpException(String s) {
        super(s, HttpResponseStatus.NOT_FOUND);
    }
}
