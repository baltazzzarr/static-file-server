package io.groggs.server.request.err;

import io.netty.handler.codec.http.HttpResponseStatus;

public class RangeNotSatisfiableHttpException extends HttpException {
    public RangeNotSatisfiableHttpException() {
        super(HttpResponseStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
    }

    public RangeNotSatisfiableHttpException(String s) {
        super(s, HttpResponseStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
    }
}
