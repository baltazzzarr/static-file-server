package io.groggs.server.request.err;

import io.netty.handler.codec.http.HttpResponseStatus;

public class NotAcceptableHttpException extends HttpException {
    public NotAcceptableHttpException() {
        super(HttpResponseStatus.NOT_ACCEPTABLE);
    }
}
