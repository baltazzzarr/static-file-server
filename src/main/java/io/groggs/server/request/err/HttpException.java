package io.groggs.server.request.err;

import io.netty.handler.codec.http.HttpResponseStatus;

public abstract class HttpException extends RuntimeException {
    protected HttpResponseStatus status;

    public HttpException(HttpResponseStatus status) {
        super(status.reasonPhrase());
        this.status = status;
    }

    public HttpException(String s, HttpResponseStatus status) {
        super(s);
        this.status = status;
    }

    public HttpException(String s, Throwable throwable, HttpResponseStatus status) {
        super(s, throwable);
        this.status = status;
    }

    public HttpResponseStatus getStatus() {
        return status;
    }
}
