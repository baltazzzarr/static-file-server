package io.groggs.server.request.err;

import io.netty.handler.codec.http.HttpResponseStatus;

public class InternalErrorHttpException extends HttpException {
    public InternalErrorHttpException() {
        super(HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }

    public InternalErrorHttpException(String s) {
        super(s, HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }

    public InternalErrorHttpException(String s, Throwable throwable) {
        super(s, throwable, HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }

    public InternalErrorHttpException(Throwable throwable) {
        super("Internal error", throwable, HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }
}
