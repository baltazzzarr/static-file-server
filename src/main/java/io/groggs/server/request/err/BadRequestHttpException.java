package io.groggs.server.request.err;

import io.netty.handler.codec.http.HttpResponseStatus;

public class BadRequestHttpException extends HttpException {
    public BadRequestHttpException() {
        super(HttpResponseStatus.BAD_REQUEST);
    }

    public BadRequestHttpException(String s) {
        super(s, HttpResponseStatus.BAD_REQUEST);
    }
}
