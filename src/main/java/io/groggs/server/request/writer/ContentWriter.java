package io.groggs.server.request.writer;

import io.netty.channel.ChannelHandlerContext;

public interface ContentWriter {
    void writeTo(ChannelHandlerContext context);
}
