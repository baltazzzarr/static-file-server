package io.groggs.server.request.writer;

import io.netty.channel.*;
import io.netty.handler.codec.http.HttpChunkedInput;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.stream.ChunkedFile;

import java.io.IOException;
import java.io.RandomAccessFile;

public class FileContentWriter implements ContentWriter {

    private static final int CHUNK_SIZE = 8192;

    private RandomAccessFile raf;
    private long length;
    private boolean keepAlive;

    public FileContentWriter(RandomAccessFile raf, long length, boolean keepAlive) {
        this.raf = raf;
        this.length = length;
        this.keepAlive = keepAlive;
    }

    @Override
    public void writeTo(ChannelHandlerContext ctx) {
        ChannelFuture sendFileFuture;
        ChannelFuture lastContentFuture;

        if (ctx.pipeline().get(SslHandler.class) == null) {
            ctx.write(new DefaultFileRegion(raf.getChannel(), 0, length), ctx.newProgressivePromise());
            lastContentFuture = ctx.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT);
        } else {
            try {
                sendFileFuture =
                        ctx.writeAndFlush(new HttpChunkedInput(new ChunkedFile(raf, 0, length, CHUNK_SIZE)),
                                ctx.newProgressivePromise());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            lastContentFuture = sendFileFuture;
        }

        if (!keepAlive) {
            lastContentFuture.addListener(ChannelFutureListener.CLOSE);
        }
    }
}
