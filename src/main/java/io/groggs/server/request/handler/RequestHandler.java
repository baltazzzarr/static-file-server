package io.groggs.server.request.handler;

import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;

public interface RequestHandler {
    WritableHttpResponse process(HttpRequest request);
    WritableHttpResponse error(HttpResponseStatus status);
}
