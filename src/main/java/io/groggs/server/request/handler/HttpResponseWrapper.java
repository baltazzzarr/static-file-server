package io.groggs.server.request.handler;

import io.groggs.server.request.writer.ContentWriter;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpUtil;

class HttpResponseWrapper implements WritableHttpResponse {
    private HttpResponse response;
    private ContentWriter contentWriter;

    private HttpResponseWrapper(HttpResponse response) {
        this.response = response;
    }

    private HttpResponseWrapper(HttpResponse response, ContentWriter contentWriter) {
        this.response = response;
        this.contentWriter = contentWriter;
    }

    static HttpResponseWrapper wrap(HttpResponse response) {
        return new HttpResponseWrapper(response);
    }

    static HttpResponseWrapper wrap(HttpResponse response, ContentWriter writer) {
        return new HttpResponseWrapper(response, writer);
    }

    public void writeResponse(ChannelHandlerContext context) {
        if (contentWriter == null) {
            ChannelFuture channelFuture = context.writeAndFlush(response);
            if (!HttpUtil.isKeepAlive(response)) {
                channelFuture.addListener(ChannelFutureListener.CLOSE);
            }
        } else {
            context.write(response);
            contentWriter.writeTo(context);
        }
    }

    @Override
    public HttpResponse getHttpResponse() {
        return response;
    }
}
