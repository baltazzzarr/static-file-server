package io.groggs.server.request.handler;

import io.groggs.server.request.err.*;
import io.groggs.server.request.writer.FileContentWriter;
import io.groggs.service.FileService;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import io.netty.util.internal.StringUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.groggs.server.request.handler.HttpResponseWrapper.wrap;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_0;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

public class SimpleRequestHandler implements RequestHandler {
    private static final String HTTP_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";
    private static final int HTTP_CACHE_SECONDS = 60;
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(HTTP_DATE_FORMAT)
            .withZone(ZoneId.of("UTC"))
            .withLocale(Locale.US);

    private final FileService fileService;

    public SimpleRequestHandler(FileService fileService) {
        this.fileService = fileService;
    }

    public WritableHttpResponse process(HttpRequest request) {
        if (!isSuccessfullyDecoded(request)) {
            throw new BadRequestHttpException("Request decoder failure");
        }

        if (!isHttpGet(request)) {
            throw new MethodNotAllowedHttpException("Method " + request.method().name() + " is not allowed");
        }

        File file = getFile(fileService.getFullPath(request.uri()));

        if (file.isDirectory()) {
            if (request.uri().endsWith("/")) {
                return wrap(listingAsHtml(request, getListOfFileNamesInDirectory(file)));
            } else {
                return wrap(redirect(request, request.uri() + '/'));
            }
        } else if (file.isFile()) {
            String ifModifiedSinceHeader = request.headers().get(HttpHeaderNames.IF_MODIFIED_SINCE);
            if (ifModifiedSince(ifModifiedSinceHeader, file.lastModified())) {
                return wrap(notModified(request));
            }

            final RandomAccessFile raf;
            long fileLength;
            try {
                raf = new RandomAccessFile(file, "r");
                fileLength = raf.length();
            } catch (FileNotFoundException ignore) {
                throw new NotFoundHttpException("There is no file located at " + file.getAbsolutePath());
            } catch (IOException e) {
                throw new InternalErrorHttpException(e);
            }

            return wrap(contentInfo(file, fileLength),
                    new FileContentWriter(raf, fileLength, HttpUtil.isKeepAlive(request)));
        } else {
            throw new ForbiddenHttpException();
        }
    }

    @Override
    public WritableHttpResponse error(HttpResponseStatus status) {
        FullHttpResponse response = new DefaultFullHttpResponse(
                HTTP_1_1, status, Unpooled.copiedBuffer("Failure: " + status + "\r\n", CharsetUtil.UTF_8));
        HttpUtil.setContentLength(response, response.content().readableBytes());
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain; charset=UTF-8");
        return wrap(response);
    }

    File getFile(String fullPath) {
        if (fullPath.isEmpty()) {
            throw new ForbiddenHttpException("File full path is empty");
        }

        File file = new File(fullPath);
        if (file.isHidden() || !file.exists()) {
            throw new NotFoundHttpException("File " + fullPath + " is hidden or doesn't exist");
        }

        return file;
    }

    private HttpResponse contentInfo(File file, long fileLength) {
        LocalDateTime now = LocalDateTime.now(ZoneId.of("UTC"));
        LocalDateTime expiration = now.plusSeconds(HTTP_CACHE_SECONDS);

        HttpResponse response = new DefaultHttpResponse(HTTP_1_1, OK);
        HttpUtil.setContentLength(response, fileLength);

        response.headers()
                .set(HttpHeaderNames.CONTENT_TYPE, fileService.getContentType(file))
                .set(HttpHeaderNames.DATE, now.format(DATE_TIME_FORMATTER))
                .set(HttpHeaderNames.EXPIRES, expiration.format(DATE_TIME_FORMATTER))
                .set(HttpHeaderNames.CACHE_CONTROL, String.format("private, max-age=%s", HTTP_CACHE_SECONDS))
                .set(HttpHeaderNames.LAST_MODIFIED,
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(file.lastModified()), ZoneId.of("UTC"))
                                .format(DATE_TIME_FORMATTER));

        return response;
    }

    private FullHttpResponse redirect(HttpRequest request, String newUri) {
        FullHttpResponse response = createFullResponse(request, FOUND);
        response.headers().set(HttpHeaderNames.LOCATION, newUri);
        return response;
    }

    private FullHttpResponse notModified(HttpRequest request) {
        FullHttpResponse response = createFullResponse(request, NOT_MODIFIED);
        String dateTime = LocalDateTime.now(ZoneId.of("UTC")).format(DATE_TIME_FORMATTER);
        response.headers().set(HttpHeaderNames.DATE, dateTime);
        return response;
    }

    private static void setConnectionHeader(HttpMessage message, boolean keepAlive) {
        if (!keepAlive) {
            message.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.CLOSE);
        } else if (message.protocolVersion().equals(HTTP_1_0)) {
            message.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
        }
    }

    private static FullHttpResponse createFullResponse(HttpRequest request, HttpResponseStatus status) {
        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, status);
        setConnectionHeader(response, HttpUtil.isKeepAlive(request));
        return response;
    }

    private boolean ifModifiedSince(String ifModifiedSinceHeader, long fileLastModified) {
        if (!StringUtil.isNullOrEmpty(ifModifiedSinceHeader)) {
            long ifModifiedSinceInSeconds = LocalDateTime
                    .parse(ifModifiedSinceHeader, DATE_TIME_FORMATTER)
                    .toInstant(ZoneOffset.UTC)
                    .toEpochMilli() / 1000 ;
            long fileLastModifiedInSeconds = fileLastModified / 1000;
            return ifModifiedSinceInSeconds == fileLastModifiedInSeconds;
        }
        return false;
    }

    private List<String> getListOfFileNamesInDirectory(File dir) {
        Predicate<File> allowedFileCondition = file -> !file.isHidden() && file.canRead()
                && fileService.isAllowedFileName(file.getName());
        return Stream.of(dir.listFiles())
                .filter(allowedFileCondition)
                .map(File::getName)
                .collect(Collectors.toList());
    }

    private HttpResponse listingAsHtml(HttpRequest request, List<String> fileNames) {
        FullHttpResponse response = createFullResponse(request, OK);
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/html; charset=UTF-8");

        StringBuilder buf = new StringBuilder()
                .append("<!DOCTYPE html>\r\n")
                .append("<html><head><meta charset='utf-8' /><title>")
                .append("Listing of: ")
                .append(request.uri())
                .append("</title></head><body>\r\n")

                .append("<h3>Listing of: ")
                .append(request.uri())
                .append("</h3>\r\n")

                .append("<ul>")
                .append("<li><a href=\"../\">..</a></li>\r\n");

        for (String name: fileNames) {
            buf.append("<li><a href=\"")
                    .append(name)
                    .append("\">")
                    .append(name)
                    .append("</a></li>\r\n");
        }

        buf.append("</ul></body></html>\r\n");

        ByteBuf buffer = Unpooled.copiedBuffer(buf, CharsetUtil.UTF_8);
        response.content().writeBytes(buffer);
        buffer.release();

        HttpUtil.setContentLength(response, response.content().readableBytes());
        return response;
    }

    private static boolean isHttpGet(HttpRequest request) {
        return HttpMethod.GET.equals(request.method());
    }

    private static boolean isSuccessfullyDecoded(HttpRequest request) {
        return request.decoderResult().isSuccess();
    }
}
