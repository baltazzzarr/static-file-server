package io.groggs.server.request.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpResponse;

public interface WritableHttpResponse {
    void writeResponse(ChannelHandlerContext context);
    HttpResponse getHttpResponse();
}
