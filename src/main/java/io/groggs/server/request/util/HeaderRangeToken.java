package io.groggs.server.request.util;

import io.groggs.server.request.err.RangeNotSatisfiableHttpException;

import java.util.Optional;

class HeaderRangeToken {
    private final Long start;
    private final Long end;

    private HeaderRangeToken(String range) {
        String[] splitResult = range.trim().split("-");
        start = parseRangeBoundary(splitResult[0]);
        end = splitResult.length > 1 ? parseRangeBoundary(splitResult[1]) : null;
    }

    static HeaderRangeToken parse(String range) {
        return new HeaderRangeToken(range);
    }

    private Long parseRangeBoundary(String boundary) {
        try {
            return boundary.isEmpty() ? null : Long.parseLong(boundary);
        } catch (NumberFormatException e) {
            throw new RangeNotSatisfiableHttpException("Cannot parse boundary " + boundary);
        }
    }

    Optional<Long> getStart() {
        return Optional.ofNullable(start);
    }

    Optional<Long> getEnd() {
        return Optional.ofNullable(end);
    }
}
