package io.groggs.server.request.util;

import io.groggs.server.request.err.RangeNotSatisfiableHttpException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RangeResolver {

    private RangeResolver() {
    }

    public static List<Range> resolve(String header, long fileLength) {
        return Stream.of(header.split(","))
                .map(HeaderRangeToken::parse)
                .map(token -> resolveRange(token, fileLength))
                .collect(Collectors.toList());
    }

    static Range resolveRange(HeaderRangeToken headerRangeToken, long fileLength) {
        if (headerRangeToken.getStart().isPresent()) {
            long start = validateAndGetLowerBound(headerRangeToken.getStart().get(), fileLength);
            long end = validateAndGetUpperBound(headerRangeToken.getEnd().orElse(fileLength), fileLength);
            return new DefaultRange(start, end);
        } else {
            long lastBytesCount = headerRangeToken.getEnd()
                    .orElseThrow(() -> new RangeNotSatisfiableHttpException("Empty range"));
            return new DefaultRange(fileLength - lastBytesCount, fileLength);
        }
    }

    private static long validateAndGetLowerBound(long bound, long max) {
        if (bound < 0) {
            throw new RangeNotSatisfiableHttpException("Negative start range bound: " + bound);
        }
        if (bound > max) {
            throw new RangeNotSatisfiableHttpException("Start bound of range is higher than total size: " + bound);
        }
        return bound;
    }

    private static long validateAndGetUpperBound(long bound, long max) {
        if (bound < 0) {
            throw new RangeNotSatisfiableHttpException("Negative range bound: " + bound);
        }
        if (bound > max) {
            return max;
        }
        return bound;
    }

}
