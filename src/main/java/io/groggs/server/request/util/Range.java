package io.groggs.server.request.util;

public interface Range {
    long getStart();
    long getEnd();
    String getStringRepresentation();
    String getStringRepresentation(long max);
}
