package io.groggs.server.request.util;

public class DefaultRange implements Range {

    private final long start;
    private final long end;

    public DefaultRange(long start, long end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public long getStart() {
        return start;
    }

    @Override
    public long getEnd() {
        return end;
    }

    @Override
    public String getStringRepresentation() {
        return String.format("%s-%s", start, end);
    }

    @Override
    public String getStringRepresentation(long max) {
        return String.format("%s/%s", getStringRepresentation(), max);
    }
}
