package io.groggs.server.request.log;

import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ObjectMessage;

public class AccessLogger {
    private static final Logger LOG = LogManager.getLogger("access");

    public static void log(HttpRequest request, HttpResponse response) {
        LOG.info(new ObjectMessage(new SimpleAccessMessage(request, response)));
    }
}
