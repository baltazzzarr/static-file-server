package io.groggs.server.request.log;

public interface AccessMessage {
    String getRemoteAddress();
    int getHttpStatus();
    long getBytesSent();
    String getUserAgent();
    String getUri();
    String getMethod();
}
