package io.groggs.server.request.log;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;

class SimpleAccessMessage implements AccessMessage {

    private HttpRequest request;
    private HttpResponse response;

    public SimpleAccessMessage(HttpRequest request, HttpResponse response) {
        this.request = request;
        this.response = response;
    }

    @Override
    public String getRemoteAddress() {
        return request.headers().get("X-Forwarded-For");
    }

    @Override
    public int getHttpStatus() {
        return response.status().code();
    }

    @Override
    public long getBytesSent() {
         try {
            return Long.parseLong(response.headers().get(HttpHeaderNames.CONTENT_LENGTH));
        } catch (NumberFormatException ignored){
            return 0L;
        }
    }

    @Override
    public String getUserAgent() {
        return request.headers().get(HttpHeaderNames.USER_AGENT);
    }

    @Override
    public String getUri() {
        return request.uri();
    }

    @Override
    public String getMethod() {
        return request.method().name();
    }

    @Override
    public String toString() {
        return "{" +
                "remoteAddress='" + getRemoteAddress() + '\'' +
                ", httpStatus=" + getHttpStatus() +
                ", bytesSent=" + getBytesSent() +
                ", userAgent='" + getUserAgent() + '\'' +
                ", uri='" + getUri() + '\'' +
                ", method='" + getMethod() + '\'' +
                '}';
    }
}
