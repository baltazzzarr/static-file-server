package io.groggs.configuration;

import org.junit.Test;

import static org.junit.Assert.*;

public class PropertySourceTest {

    private PropertySource propertySource = new PropertySource();

    @Test
    public void getPropertyAsStringFromSystem() {
        System.setProperty("root.path", "/root");
        assertEquals("/root", propertySource.getPropertyAsString("root.path"));
    }

    @Test(expected = PropertyNotFoundException.class)
    public void getPropertyAsStringNotFound() {
        propertySource.getPropertyAsString("some.missing.property");
    }

    @Test
    public void getPropertyAsInteger() {
        assertEquals(8081, propertySource.getPropertyAsInteger("server.port"));
    }
}
