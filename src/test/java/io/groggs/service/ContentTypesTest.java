package io.groggs.service;

import org.junit.Test;

import java.io.File;

import static com.google.common.net.MediaType.PDF;
import static com.google.common.net.MediaType.PLAIN_TEXT_UTF_8;
import static org.junit.Assert.*;

public class ContentTypesTest {

    @Test
    public void getContentTypeByExtension() {
        assertEquals(PDF.toString(), ContentTypes.getContentType(new File("book.pdf")));
        assertEquals(PLAIN_TEXT_UTF_8.toString(), ContentTypes.getContentType(new File("note.txt")));
    }

    @Test
    public void getContentTypeFromName() {
        assertEquals("application/x-shockwave-flash", ContentTypes.getContentType(new File("idk.swf")));
        assertEquals("audio/vnd.dlna.adts", ContentTypes.getContentType(new File("hello.aac")));
    }
}
