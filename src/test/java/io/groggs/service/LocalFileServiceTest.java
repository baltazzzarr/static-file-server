package io.groggs.service;

import io.groggs.configuration.PropertySource;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class LocalFileServiceTest {

    private LocalFileService localPathService = new LocalFileService(new PropertySource());

    @Test
    public void getFullPath() throws UnsupportedEncodingException {
        String uri = URLEncoder.encode("/folder/subfolder/file", "UTF-8");
        System.out.println(localPathService.getFullPath(uri));
    }
}
