package io.groggs.server.request.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class HeaderRangeTokenTest {
    @Test
    public void getNormalRange() {
        HeaderRangeToken range = HeaderRangeToken.parse("1234-5464");
        assertTrue(range.getStart().isPresent());
        assertTrue(range.getEnd().isPresent());
    }

    @Test
    public void getOnlyLastBytes() {
        HeaderRangeToken range = HeaderRangeToken.parse("1234-5464");
        assertFalse(range.getStart().isPresent());
        assertTrue(range.getEnd().isPresent());
    }

    @Test
    public void getOnlyFirstBytes() {
        HeaderRangeToken range = HeaderRangeToken.parse("1234-5464");
        assertTrue(range.getStart().isPresent());
        assertFalse(range.getEnd().isPresent());
    }
}
