package io.groggs.server.request.util;

import io.groggs.server.request.err.RangeNotSatisfiableHttpException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class RangeResolverTest {

    @Test
    public void resolve() {
        List<Range> result = RangeResolver.resolve("123-567,899-1258", 3500L);
        assertEquals(123, result.get(0).getStart());
        assertEquals(567, result.get(0).getEnd());
        assertEquals(899, result.get(1).getStart());
        assertEquals(1258, result.get(1).getEnd());
    }

    @Test
    public void resolveRange() {
        Range result = RangeResolver.resolveRange(HeaderRangeToken.parse("123-456"), 8888L);
        assertEquals(123L, result.getStart());
        assertEquals(456L, result.getEnd());
    }

    @Test
    public void resolveRangeWithMissingUpperBound() {
        Range result = RangeResolver.resolveRange(HeaderRangeToken.parse("123-"), 8888L);
        assertEquals(123L, result.getStart());
        assertEquals(8888L, result.getEnd());
    }

    @Test
    public void resolveRangeWithMissingLowerBound() {
        Range result = RangeResolver.resolveRange(HeaderRangeToken.parse("-333"), 8888L);
        assertEquals(8888L - 333L, result.getStart());
        assertEquals(8888L, result.getEnd());
    }

    @Test(expected = RangeNotSatisfiableHttpException.class)
    public void resolveRangeWithLowerBoundHigherThanTotal() {
        RangeResolver.resolveRange(HeaderRangeToken.parse("333-"), 332L);
    }
}
